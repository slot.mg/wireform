terraform {
  required_version = "1.0.4"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.54.0"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "3.1.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "2.1.0"
    }

    time = {
      source  = "hashicorp/time"
      version = "0.7.2"
    }

    nullresource = {
      source  = "hashicorp/null"
      version = "3.1.0"
    }
  }
}

provider "aws" {
  region  = var.aws_regiao
  profile = var.aws_profile
}

resource "aws_security_group" "wireguard-sg" {
  name        = "wireguard-sg"
  description = "SG Wireguard"
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description      = "SAIDA"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = var.aws_tag.Name
  }
}

resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "aws_key_pair" "generated_key" {
  key_name   = "wireguard"
  public_key = tls_private_key.ssh_key.public_key_openssh
  provisioner "local-exec" {
    command = "echo '${tls_private_key.ssh_key.private_key_pem}' > /tmp/wireguard.pem"
  }
  provisioner "local-exec" {
    command = "chmod 400 /tmp/wireguard.pem"
  }
  tags = {
    Name = var.aws_tag.Name
  }
}

resource "aws_instance" "wireguard" {
  ami                    = var.aws_ami
  instance_type          = var.aws_instancia_tipo
  vpc_security_group_ids = [aws_security_group.wireguard-sg.id]
  security_groups        = [aws_security_group.wireguard-sg.name]
  key_name               = aws_key_pair.generated_key.key_name

  tags = {
    Name = "wireguard"
  }
  depends_on = [aws_security_group.wireguard-sg, aws_key_pair.generated_key]
}

resource "local_file" "hosts_cfg" {
  content    = templatefile("hosts.tpl", { wireguard = aws_instance.wireguard.public_ip })
  filename   = "ansible/hosts"
  depends_on = [aws_security_group.wireguard-sg, aws_key_pair.generated_key, aws_instance.wireguard]
}

resource "time_sleep" "wait" {
  depends_on      = [aws_instance.wireguard, local_file.hosts_cfg]
  create_duration = "35s"
}

resource "null_resource" "ansible" {
  provisioner "local-exec" {
    command = "ansible-playbook ansible/instala.yml"
  }
  depends_on = [time_sleep.wait, local_file.hosts_cfg]
}
