output "ip" {
  value = aws_instance.wireguard.public_ip
}

output "sg-name" {
  value = aws_security_group.wireguard-sg.name
}

output "sg-id" {
  value = aws_security_group.wireguard-sg.id
}

output "key-nome" {
  value = aws_key_pair.generated_key.key_name
}