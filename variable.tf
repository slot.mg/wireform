variable "aws_regiao" {
  type        = string
  description = "Região selecionada"
  default     = "us-east-1"
}

variable "aws_profile" {
  type        = string
  description = "Profile utilizado pelo awscli"
  default     = "default"
}

variable "aws_ami" {
  type        = string
  description = "AMI utilizada"
  default     = "ami-051601e49261d6e57" #"debian-11-arm64-daily-20210816-736"
}

variable "aws_instancia_tipo" {
  type        = string
  description = "Tipo de instancia"
  default     = "t4g.micro"
}

variable "aws_ec2_chave_privada" {
  type        = string
  description = "Chave privada para conexão"
  default     = "wireguard"
}

variable "aws_tag" {
  type        = map(string)
  description = "Tags setadas para o projeto"
  default = {
    Name    = "wireguard"
    Projeto = "wireguard"
    Env     = "DEV"
  }
}
